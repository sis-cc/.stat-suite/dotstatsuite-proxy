FROM node:18-alpine

ARG GIT_HASH

RUN mkdir -p /opt
WORKDIR /opt

RUN echo $GIT_HASH

COPY package.json yarn.lock /opt/
COPY dist /opt/dist
COPY data  /opt/data

RUN yarn install --production && yarn cache clean

ENV GIT_HASH=$GIT_HASH

EXPOSE 80
CMD yarn dist:run
