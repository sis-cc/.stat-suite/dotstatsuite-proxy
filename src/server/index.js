import 'core-js/stable'
import 'regenerator-runtime/runtime'
import debug from './debug'
import { initResources } from 'jeto'
import initMongo from './init/mongo'
import initHttp from './init/http'
import initRouter from './init/router'
import initConfig from './init/config'
import initRoutes from './init/routes'
import initServices from './services'
import initRateLimiter from './init/rateLimiter'

const resources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initMongo,
  initRateLimiter,
  initRoutes,
  initServices,
  initRouter,
  initHttp,
]

initResources(resources)
  .then(() => debug.info('🚀 server started'))
  .catch(err => {
    debug.error(err, 'Boot failed')
    process.exit()
  })
