import debug from '../debug'

const NAME = 'routes'

const Model = ctx => {
  const { mongo } = ctx()
  const collection = mongo.database.collection(NAME)

  return {
    collection,
  }
}

export default async ctx => {
  debug.info(`model "${NAME}" up`)
  return [NAME, new Model(ctx)]
}
