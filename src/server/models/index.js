import { reduce } from 'ramda'
import Route from './routes'

const providers = [Route]

export default async ctx =>
  reduce(
    async (acc, provider) => {
      const res = await acc
      const [name, model] = await provider(ctx)
      return { ...res, [name]: model }
    },
    Promise.resolve({}),
    providers,
  )
