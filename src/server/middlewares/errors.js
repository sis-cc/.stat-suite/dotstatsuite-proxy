import debug from '../debug'

const error = (err, req, res, next) => {
  if (!err) return next()
  debug.error(err.stack)
  const code = err.code || 500
  if (err.message) return res.status(code).json({ message: err.message })
  return res.sendStatus(code)
}

module.exports = error
