import { initResources } from 'jeto'
import { v4 as uuid } from 'uuid'
import nock from 'nock'
import initMongo from '../init/mongo'
import initHttp from '../init/http'
import initRouter from '../init/router'
import initServices from '../services'
import initRateLimiter from '../init/rateLimiter'

export const ROUTE1 = { host: '0.0.0.0', target: 'http://app', tenant: 'member1' }
export const ROUTE2 = { host: '0.0.0.0', target: 'http://app1', path: '/test1', ratePoints: 51 }

const initRoutes = async ctx => {
  const routes = () => [ROUTE1, ROUTE2]
  return ctx({ routes })
}

const CONFIG_URL = 'http://config'

export const doBeforeAll = async () => {
  let CTX
  const resources = [initConfig, initMongo, initRateLimiter, initRoutes, initServices, initRouter, initHttp]
  try {
    await initResources(resources).then(async ctx => {
      CTX = ctx
    })

    nock(CONFIG_URL).persist().get('/assets/member1/scope1/resource.json').reply(200, { data: 1 })

    nock(CONFIG_URL).persist().get('/healthcheck').reply(200)

    nock('http://app').persist().get('/test').reply(200)
    nock('http://app1').persist().get('/').reply(200)

    return CTX
  } catch (err) {
    console.error(err) // eslint-disable-line
    if (CTX) {
      const { mongo } = CTX()
      if (mongo) await mongo.dropDatabase().then(() => mongo.close())
    }
  }
}

export const doAfterAll = ctx => {
  const {
    mongo,
    httpServer,
    config: {
      mongo: { dbName },
    },
  } = ctx()
  try {
    mongo
      .dropDatabase(`${dbName}:test`)
      .then(() => mongo.close())
      .then(() => httpServer.close())
  } catch (err) {
    console.error(err) // eslint-disable-line
  }
}

export const initConfig = ctx =>
  ctx({
    config: {
      apiKey: 'secret',
      gitHash: 'None',
      gitTag: 'None',
      version: '0.0.0',
      configUrl: CONFIG_URL,
      httpServer: { host: '0.0.0.0' },
      mongo: {
        url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
        dbName: `test-${uuid()}`,
      },
      rateLimiter: {
        points: 100,
        duration: 1,
      },
    },
  })
