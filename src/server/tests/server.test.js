import got from 'got'
import urljoin from 'url-join'
import { doBeforeAll, doAfterAll } from './utils'

let CTX
const sleep = delay => new Promise(resolve => setTimeout(resolve, delay))

describe('Assets', () => {
  beforeAll(async () => {
    CTX = await doBeforeAll()
  })

  afterAll(() => doAfterAll(CTX))

  it('Should route assets', async () => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '/assets', '/member1/scope1/resource.json')
    await got.get(url)
  })

  it('Should not route assets', done => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '/assets', '/member1/fake/resource.json')
    got.get(url).catch(() => done())
  })

  it('Should healthcheck', async () => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '_healthcheck_')
    const res = await got.get(url).json()
    console.log(res)
    expect(res.gitHash).toEqual('None')
    expect(res.gitTag).toEqual('None')
    expect(res.version).toEqual('0.0.0')
  })

  it('Should route', async () => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '/test')
    await got.get(url).json()
  })

  it('Should not route', async () => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '/test1')
    await got.get(url)
    return expect(() => got.get(url).json()).rejects.toThrow(/429/)
  })

  it('Should route again', async () => {
    const { httpServer } = CTX()
    const url = urljoin(httpServer.url, '/test1')
    await sleep(1200)
    await got.get(url)
  })
})
