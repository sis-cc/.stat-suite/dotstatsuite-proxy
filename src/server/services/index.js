import initHealthcheck from './healthcheck'

export default ctx => initHealthcheck(ctx)
