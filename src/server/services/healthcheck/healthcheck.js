import debug from '../../debug'
import got from 'got'
import urljoin from 'url-join'

const NAME = 'healthcheck'

async function doHealthcheck() {
  const {
    startTime,
    config: { configUrl, gitHash, gitTag, version },
    mongo,
    routes,
  } = this.globals()

  const res = {
    startTime,
    gitHash,
    gitTag,
    version,
    routes: routes().length,
    mongo: 'OK',
    configServer: 'OK',
  }

  try {
    await got.get(urljoin(configUrl, 'healthcheck'))
  } catch (err) {
    debug.error(err)
    res.configServer = 'KO'
  }

  try {
    await mongo.ping()
  } catch (err) {
    debug.error(err)
    res.mongo = 'KO'
  }

  return res
}

const healthcheck = {
  get: doHealthcheck,
}

export default evtx => evtx.use(NAME, healthcheck)
