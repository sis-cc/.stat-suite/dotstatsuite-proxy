import debug from '../../debug'
import evtX from 'evtx'
import initHealthcheck from './healthcheck'
import { initServices } from '../utils'

const services = [initHealthcheck]

export default ctx => {
  const healthcheck = evtX(ctx).configure(initServices(services))
  debug.info('healthcheck service up.')
  return ctx({ services: { ...ctx().services, healthcheck } })
}
