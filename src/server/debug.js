const pino = require('pino')
const pretty = require('pino-pretty')
const stackdriver = require('pino-stackdriver')

const app = 'proxy'
const level = process.env.NODE_ENV === 'test' ? 'silent' : process.env.LOGGING_LEVEL || 'info'
const streams = [{ stream: pretty() }]

if (process.env.LOGGING_DRIVER === 'gke') {
  const projectId = process.env.LOGGING_PROJECT_ID
  const logName = process.env.LOGGING_LOGNAME
  const stackDriverStream = stackdriver.createWriteStream({
    projectId,
    logName,
    resource: {
      type: 'global',
      labels: { app },
    },
  })
  streams.push({ level, stream: stackDriverStream })
}

module.exports = pino({ name: app, level }, pino.multistream(streams))
