require('../env')
import meta from '../../../package.json'

export default ctx => {
  const httpServer = { host: process.env.SERVER_HOST || '0.0.0.0', port: Number(process.env.SERVER_PORT) || 80 }
  const config = {
    isProduction: process.env.NODE_ENV === 'production',
    httpServer,
    gitHash: process.env.GIT_HASH || 'local',
    gitTag: process.env.GIT_TAG || 'local',
    version: meta.version,
    configUrl: process.env.CONFIG_URL,
    apiKey: process.env.API_KEY,
    rateLimiter: {
      dbName: process.env.RATE_LIMITER_MONGODB_DATABASE || 'rate-limiter',
      points: Number(process.env.RATE_LIMITER_POINTS || 100),
      duration: Number(process.env.RATE_LIMITER_DURATION || 1),
    },
    mongo: {
      url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
      dbName: process.env.MONGODB_DATABASE || 'proxy',
    },
  }

  return ctx({ startTime: new Date(), config })
}
