import express from 'express'
import { propOr, sort, allPass, is, find, replace } from 'ramda'
import path from 'path'
import compression from 'compression'
import bodyParser from 'body-parser'
import httpProxy from 'http-proxy'
import urljoin from 'url-join'
import UrlPattern from 'url-pattern'
import debug from '../debug'
import healthcheckConnector from '../services/healthcheck/connector'
import errorHandler from '../middlewares/errors'

const expressPino = require('express-pino-logger')({ logger: debug })

const proxy = httpProxy.createProxyServer({ xfwd: true, ws: true })

const makeTargetAsset = (configUrl, url) => urljoin(configUrl, '/assets', url)

const getIp = req => {
  if (req.headers['x-forwarded-for']) return req.headers['x-forwarded-for'].split(',')[0]
  return req.hostname
}

const getProto = req => {
  if (req.headers['x-forwarded-proto']) return req.headers['x-forwarded-proto']
  return req.protocol
}

const redirectHttps = req => {
  if (getProto(req) === 'http') return `https://${req.headers.host}${req.url}`
}

const notFound = (res, target) => err => {
  debug.warn(`cannot route to "${target}": ${err.code}`)
  res.statusCode = 404
  res.end()
}

const isPattern = path => path && path.indexOf(':') !== -1
const makeApp = ({ tenant, path, ...others }, url) => {
  const app = { tenant, path, ...others }

  if (isPattern(path)) {
    const res = new UrlPattern(`${path}/*`).match(url)
    for (const key in res) app[key] = res[key]
    app.targetPath = `/${res._}`
  }

  if (is(Function, tenant)) {
    const computedTenant = tenant(app)
    app.tenant = computedTenant
  }

  return app
}

const makeTarget = ({ path, targetPath, target, tenant }, url) => {
  let [newUrl, str] = url.split('?')
  const params = new URLSearchParams(str)
  if (tenant && params.has('tenant')) params.delete('tenant')
  if (path) newUrl = isPattern(path) ? targetPath : replace(new RegExp(`^${path}`), '', newUrl)
  const query = params.toString() ? `?${params.toString()}` : ''
  return urljoin(target, newUrl, query)
}

const matchPath = url => route => {
  if (!route.path) return true
  if (isPattern(route.path)) return new UrlPattern(`${route.path}/*`).match(url)
  return new RegExp(`^${route.path}`).test(url)
}

const matchHost = host => route => new RegExp(`^${route.host}`).test(host)

const findRoute = (routes, { url, host }) => find(allPass([matchHost(host), matchPath(url)]), routes)

export const getRoutes =
  routes =>
  async ({ url, headers: { host } }) => {
    const sortedRoutes = sort((a, b) => (propOr('', 'path', a) > propOr('', 'path', b) ? -1 : 1), routes())
    const entry = await findRoute(sortedRoutes, { url, host })
    if (!entry) return
    const app = makeApp(entry, url)
    const target = makeTarget(app, url)
    const res = {
      target,
      forceHttps: app.forceHttps,
      ratePoints: app.ratePoints || 1,
    }
    if (app.tenant) res.headers = { 'x-tenant': app.tenant }
    return res
  }

const routeAssets =
  ({ configUrl }) =>
  (req, res) => {
    const target = makeTargetAsset(configUrl, req.path)
    debug.info({ from: `${req.hostname}${req.path}`, to: `${target}` }, `asset routed`)
    return proxy.web(req, res, { ignorePath: true, target }, notFound(res, target))
  }

const appRoute =
  ({ rateLimiter, Routes }) =>
  async (req, res) => {
    const t0 = new Date()
    const route = await Routes(req)

    if (!route) {
      debug.warn(`unknown target route for "${req.hostname}${req.path}"`)
      res.statusCode = 404
      return res.end()
    }

    if (route.forceHttps) {
      const redirect = redirectHttps(req)
      if (redirect) {
        debug.info(`redirect "${req.hostname}${req.path}" to "${redirect}"`)
        res.writeHead(301, { Location: redirect })
        return res.end()
      }
    }
    const ip = getIp(req)
    try {
      const rateLimiterRes = await rateLimiter.consume(ip, route.ratePoints)
      const logMessage = {
        'X-forwarded-for': ip,
        from: `${req.hostname}${req.path}`,
        to: `${route.target}`,
        'X-RateLimit-Limit': rateLimiter.points,
        'X-RateLimit-Remaining': rateLimiterRes.remainingPoints,
        'X-RateLimit-Reset': new Date(Date.now() + rateLimiterRes.msBeforeNext),
      }

      if (route.headers?.['x-tenant']) {
        logMessage.tenant = `${route.headers['x-tenant']}`
        debug.info(logMessage, `route in ${new Date() - t0} ms`)
      } else {
        debug.info(logMessage, `route in ${new Date() - t0} ms`)
      }

      proxy.web(
        req,
        res,
        { ignorePath: true, target: route.target, headers: route.headers },
        notFound(res, route.target),
      )
    } catch (err) {
      const logMessage = {
        'X-forwarded-for': ip,
        from: `${req.hostname}${req.path}`,
        to: `${route.target}`,
        'X-RateLimit-Limit': rateLimiter.points,
        'X-RateLimit-Consumed': err.consumedPoints,
        'X-RateLimit-Remaining': err.remainingPoints,
      }
      debug.warn(logMessage, `route rejected: Too Many Requests`)
      res.writeHead(429, { 'Retry-After': new Date(Date.now() + err.msBeforeNext) })
      res.end()
    }
  }

export default async ctx => {
  const {
    routes,
    services: { healthcheck },
    config: { configUrl },
    rateLimiter,
  } = ctx()

  const Routes = getRoutes(routes)

  const app = express()
  app.use('/assets', routeAssets({ configUrl }))
  app.use('/_healthcheck_', healthcheckConnector(healthcheck))
  app.use(expressPino)
  app.use(compression())
  app.use(appRoute({ rateLimiter, Routes }))
  app.use(errorHandler)

  debug.info('HTTP routes setup')
  return ctx({ app })
}
