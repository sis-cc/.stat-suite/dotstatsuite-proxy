import { RateLimiterMongo, RateLimiterMemory } from 'rate-limiter-flexible'
import debug from '../debug'

export default ctx => {
  const {
    config: { rateLimiter: rateLimiterConfig },
    mongo,
  } = ctx()

  const rateLimiterMemory = new RateLimiterMemory({
    points: rateLimiterConfig.points,
    duration: rateLimiterConfig.duration,
  })

  const opts = {
    ...rateLimiterConfig,
    storeClient: mongo.client,
    inmemoryBlockOnConsumed: rateLimiterConfig.points + 1,
    insuranceLimiter: rateLimiterMemory,
  }

  const rateLimiterMongo = new RateLimiterMongo(opts)

  debug.info('Rate Limiter is on')

  return ctx({ rateLimiter: rateLimiterMongo })
}
