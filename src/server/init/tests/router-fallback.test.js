import { getRoutes } from '../router'

const APPS = [
  //{ host: 'h-dlm', target: 'http://dlm', forceHttps: true },
  { host: 'h-dlm', target: 'http://dlm', tenant: 'a:1' },
  //{ host: 'h-dlm2', target: 'http://dlm', forceHttps: true },
  //{ host: 'h-dlm2', target: 'http://dlm', forceHttps: true, tenant: 'a:2' },
]

let Routes = getRoutes(() => APPS)
const defaultRes = { target: 'http://dlm', ratePoints: 1 }

describe('Router', () => {
  it('should fallback if no param', async () => {
    const req = { url: '', headers: { host: 'h-dlm' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'a:1' },
      ...defaultRes,
    }
    expect(app).toEqual(res)
  })

  xit('should override if params', async () => {
    const req = { url: '?tenant=z:9', headers: { host: 'h-dlm' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'z:9' },
      ...defaultRes,
    }
    expect(app).toEqual(res)
  })
})
