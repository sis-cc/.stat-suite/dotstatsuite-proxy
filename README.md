# Proxy server

The entry point of an Kubernetes cluster is an `ingress` service, that should allow to route http requests to desired applications.

`ingress` have limited features, so to satisfy our needs we are using a home made `proxy` service behind GCE ingress.

Let's say `app1`, `app2` and `app3` are deployed in the cluster respectively on both `staging` and `qa` environments.

The 2 first apps belong to tenant `t1`, the latter for `t2`.

- url to acces `app1` in staging will be: https://t1.siscc.org/staging/app1
- on `qa`: https://t1.siscc.org/qa/app1
- `app2` on `staging` https://t2.siscc.org/staging/app2

```
  https://<tenant>.siscc.org/<environment>/<application>
```

REM: `<tenant>.siscc.org` could be replaced by a dedicaded DNS entry in tenant's DNS pointing to `kube-rp` @IP

So we have 2 needs:

- route request depending on (`host`, `path`) urls
- set tenants headers depending on `host` to instruct target application of the tenant

## Docker Images

_siscc/dotstatsuite-proxy_

## Probe

```
GET /_healthcheck_
{
  "gitHash": "f5a950ca",
  "gitTag": "causality",
  "version": "25.0.0",
  "startTime": "2019-03-26T08:10:07.391Z"
}
```

## Gitlab

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy/

## Usage

### Development

Clone repo

```
$ yarn
$ PORT=5007 yarn start:srv
```

## Settings

To add an access to a new application `app1` deployed in `staging` environment on `kube-rp`:

Add entries in `data/routes.json`

```
[
  {
    "host": "kube.tenant1.com",
    "target": "http://app1.staging.svc.cluster.local"
    "tenant": tenant1",
    "path": "/sfs",
    "forceHttps": true,
    "ratePoints": 5
  },
  ...
]
```

1. `kube.tenant1.com` is a tenan1's DNS entry linked to kube-rp@IP
2. `app1` in `target` must match a service name deployed in `staging`
3. `tenant` will be added to destination request to the target service, here `tenant1` may help to get `config` resources.
4. `forceHttps` will redirect if req.headers['x-forwarded-proto'] === 'http' CARE it only works behind a proxy
5. `ratePoints` is the route number of points for a route

### Rate Limiter

`proxy` counts and limits number of routes to manage by ip address and protects from DDoS and brute force attacks at any scale.

It works with MongoDB and in memory as fallback and allows to control requests rate.

Each route costs a number of points (default === 1)

For each request (except '/assets') a rate limiter counter, with an initial value of process.env.RATE_LIMITER_POINTS, is evaluated per IP address, where counter(IP) -= route.points within the time window defined by process.env.RATE_LIMITER_DURATION

When a counter becomes negative, proxy returns status code 429

Env variables:

```
RATE_LIMITER_MONGODB_DATABASE || 'rate-limiter',
RATE_LIMITER_POINTS || 100,
RATE_LIMITER_DURATION || 1,
```

### Routes management

For a given host:

- Always only the first route is routes.json is applied. Subsequent routes for the same host are therefor ignored.
- Whenever the proxy could not determine the tenant or has determined a tenant to be used, but it is not defined in tenants.json, then an error is thrown.
- If the first route has a tenant defined then it is being used.
- If the first route doesn't have a tenant defined then it is taken from the query URL if present, otherwise defaults are used if defined.
- The default organisation of a tenant can be defined in tenants.json.
- The default scope of a tenant is hardcoded per app: 'de' for the Data Explorer and 'dlm' for the Data Lifecycle Manager.

Example 1:

```json
{"host": "dlm-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager" },
{"host": "dlm-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager", "tenant": "siscc:dlm" },
{"host": "dlm2-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager" },
{"host": "dlm2-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager", "tenant": "siscc:dlm2" },
```

- routes 2 and 4 are ignored
- dlm-qa.siscc.org --\> will use `siscc:dlm` (default organisation (siscc) and default scope (dlm));
- dlm-qa.siscc.org?tenant=oecd:archive --\> will use `oecd:archive` from query parameter
- dlm2-qa.siscc.org --\> will use `siscc:dlm` (default organisation (siscc) and default scope (dlm));
- dlm2-qa.siscc.org?tenant=oecd:archive --\> will use `oecd:archive` from query parameter

Example 2:

```json
{"host": "dlm-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager" },
{"host": "dlm2-qa.siscc.org", "forceHttps": "true", "target": "http://data-lifecycle-manager", "tenant": "siscc:dlm2" },
```

- dlm-qa.siscc.org --\> will use `siscc:dlm` (default organisation (siscc) and default scope (dlm))
- dlm-qa.siscc.org?tenant=oecd:archive --\> will use `oecd:archive` from query parameter
- dlm2-qa.siscc.org --\> will use the defined tenant `siscc:dlm2`
- dlm2-qa.siscc.org?tenant=oecd:archive --\> will use the defined tenant `siscc:dlm2`; the query parameter is ignored
